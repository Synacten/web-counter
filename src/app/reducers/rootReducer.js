import {
  HANDLEREST,
  HANDLETRAIN,
  HANDLESTEP,
  RUNTIME,
  PAUSE,
  CLEARALLINTERVALS,
} from '../actions/types';

const initialState = {
  step: 2,
  train: 5,
  rest: 3,
  pause: false,
  runtime: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case HANDLEREST:
      return {
        ...state,
        rest: action.rest,
      };
    case HANDLETRAIN:
      return {
        ...state,
        train: action.train,
      };
    case HANDLESTEP:
      return {
        ...state,
        step: action.step,
      };
    case RUNTIME:
      return {
        ...state,
        runtime: action.payload,
      };
    case PAUSE:
      return {
        ...state,
        pause: action.payload,
      };
    default:
      return state;
  }
}
