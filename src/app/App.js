import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import store from './store/store';
import { Nav } from './components/layout/Nav';
import { Counter } from './Counter';
import Stepcounter from './Stepcounter';

const App = () => (
  <div>
    <Provider store={store}>
      <BrowserRouter>
        <Nav />
        <Switch>
          <Route path="/labcounter" component={Counter} />
          <Route path="/yogacounter" component={Stepcounter} />
        </Switch>
      </BrowserRouter>
    </Provider>
  </div>
);

ReactDOM.render(<App />, document.querySelector('.root'));
