import {
  HANDLEREST,
  HANDLETRAIN,
  HANDLESTEP,
  RUNTIME,
} from './types';
import audioRest from '../mp3/kirk01.mp3';
import endTrain from '../mp3/spock05.mp3';
import audioFile from '../mp3/spock02.mp3';

export const handleRest = (rest) => async (dispatch) => {
  dispatch({
    type: HANDLEREST,
    rest: Number.isNaN(rest) ? 0 : rest,
  });
};
export const handleTrain = (train) => async (dispatch) => {
  dispatch({
    type: HANDLETRAIN,
    train: Number.isNaN(train) ? 0 : train,
  });
};

export const handleStep = (step) => async (dispatch) => {
  dispatch({
    type: HANDLESTEP,
    step: Number.isNaN(step) ? 0 : step,
  });
};

export const reduceTime = (rest, train, step) => async (dispatch) => {
  dispatch({
    type: RUNTIME,
    payload: true,
  });
  const initialState = {};
  if (Object.keys(initialState).length < 1) {
    Object.assign(initialState, { rest, train, step });
  }
  let restLocal = rest;
  let trainLocal = train;
  let stepLocal = step;

  const restInterval = () => {
    dispatch({
      type: HANDLETRAIN,
      train: trainLocal,
    });
    dispatch({
      type: HANDLEREST,
      rest: restLocal,
    });
    const restAudioStart = new Audio(audioRest);
    restAudioStart.play();
    const restLocalInterval = setInterval(() => {
      restLocal -= 1;
      dispatch({
        type: HANDLEREST,
        rest: restLocal,
      });
      if (restLocal <= 0) {
        const audio = new Audio(audioFile);
        audio.play();
        clearInterval(restLocalInterval);
        trainInterval();
      }
    }, 1000);
  };

  restInterval();

  const trainInterval = () => {
    const innerTrainInterval = setInterval(() => {
      trainLocal -= 1;
      dispatch({
        type: HANDLETRAIN,
        train: trainLocal,
      });
      if (trainLocal <= 0) {
        clearInterval(innerTrainInterval);
        if (stepLocal > 0) {
          stepLocalReduce();
          console.log('change step');
        }
      }
      if (restLocal < 1 && trainLocal < 1 && stepLocal < 1) {
        const end = new Audio(endTrain);
        end.play();
        dispatch({
          type: RUNTIME,
          payload: false,
        });
        dispatch({
          type: HANDLESTEP,
          step: initialState.step,
        });
        dispatch({
          type: HANDLETRAIN,
          train: initialState.train,
        });
        dispatch({
          type: HANDLEREST,
          rest: initialState.rest,
        });
      }
    }, 1000);
  };

  const stepLocalReduce = () => {
    stepLocal -= 1;
    restLocal = initialState.rest;
    trainLocal = initialState.train;
    console.log('change step', restLocal, trainLocal, stepLocal);
    restInterval();
    dispatch({
      type: HANDLESTEP,
      step: stepLocal,
    });
  };
};

export const resetTime = () => async (dispatch) => {
  dispatch({
    type: HANDLEREST,
    rest: 0,
  });
  dispatch({
    type: HANDLETRAIN,
    train: 0,
  });
  dispatch({
    type: HANDLESTEP,
    step: 0,
  });
};

export const reduceAllTime = ({ rest, train, step }) => async (dispatch) => {
  console.log(rest, train, step);
};
