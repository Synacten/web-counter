export const HANDLEREST = 'HANDLEREST';
export const HANDLETRAIN = 'HANDLETRAIN';
export const HANDLESTEP = 'HANDLESTEP';

export const REDUCETIME = 'REDUCETIME';

export const RUNTIME = 'RUNTIME';
export const PAUSE = 'PAUSE';

export const CLEARALLINTERVALS = 'CLEARALLINTERVALS';
