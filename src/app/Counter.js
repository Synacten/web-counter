import React, { useState } from 'react';
import {
  CounterRoot, Btnincrement, Btndecrement, Reset, Clicked,
} from './style/Style';

export const Counter = () => {
  const [count, setCount] = useState(0);
  const increment = () => {
    setCount(count + 1);
    window.navigator.vibrate(200);
  };
  const decrement = () => {
    setCount(count - 1);
  };
  const resetCount = () => {
    setCount(0);
  };
  return (
    <CounterRoot>
      <Clicked>{count}</Clicked>
      <Btnincrement onClick={increment} role="presentation">+1</Btnincrement>
      <Btndecrement onClick={decrement} role="presentation">-1</Btndecrement>
      <Reset onClick={resetCount} role="presentation">Сброс</Reset>
    </CounterRoot>
  );
};
