/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import Cleave from 'cleave.js/react';
import { connect } from 'react-redux';
import { StepStyled } from './style/Style';
import {
  handleRest,
  handleTrain,
  handleStep,
  reduceTime,
  resetTime,
} from './actions/actions';

const Stepcounter = ({
  rest,
  step,
  train,
  handleRest: _handleRest,
  handleTrain: _handleTrain,
  handleStep: _handleStep,
  reduceTime: _reduceTime,
  resetTime: _resetTime,
  runtime,
  pause,
}) => {
  const startingTrain = (e) => {
    e.preventDefault();
    if (rest <= 0 || train < 1) {
      return null;
    }
    return _reduceTime(rest, train, step);
  };

  const num = (nums) => parseFloat(nums);

  const changeRest = (e) => {
    _handleRest(num(e.target.value));
  };

  const changeTrain = (e) => {
    _handleTrain(num(e.target.value));
  };

  const changeStep = (e) => {
    _handleStep(num(e.target.value));
  };


  return (
    <StepStyled>
      <form onSubmit={startingTrain}>
        <label htmlFor="step">
          Step count (times)
          <Cleave
            type="tel"
            className="score-wrap"
            autoComplete="phone"
            placeholder="-"
            name="step"
            value={step}
            onChange={changeStep}
            readOnly={runtime || pause}
            options={{
              blocks: [3],
              delimiters: [],
              numericOnly: true,
            }}
          />
        </label>
        <label htmlFor="train">
          Train count (seconds)
          <Cleave
            type="tel"
            className="score-wrap"
            autoComplete="phone"
            placeholder="-"
            name="step"
            value={train}
            onChange={changeTrain}
            readOnly={runtime || pause}
            options={{
              blocks: [3],
              delimiters: [],
              numericOnly: true,
            }}
          />
        </label>
        <label htmlFor="rest">
          Rest count (seconds)
          <Cleave
            type="tel"
            className="score-wrap"
            autoComplete="phone"
            placeholder="-"
            name="step"
            value={rest}
            onChange={changeRest}
            readOnly={runtime || pause}
            options={{
              blocks: [3],
              delimiters: [],
              numericOnly: true,
            }}
          />
        </label>
        <label htmlFor="submit">
          <input type="submit" value={runtime ? 'Running...' : 'Start train'} name="start" disabled={runtime || pause} />
        </label>
      </form>
      <div className="add-btn">
        <span onClick={() => console.log('change')} role="presentation">
          Pause
        </span>
        <span onClick={() => window.location.reload()} role="presentation">
          Reset
        </span>
      </div>
    </StepStyled>
  );
};

const mapDispatchToProps = (state) => ({
  step: state.root.step,
  train: state.root.train,
  rest: state.root.rest,
  runtime: state.root.runtime,
  pause: state.root.pause,
});

export default connect(mapDispatchToProps, {
  handleRest,
  handleTrain,
  handleStep,
  reduceTime,
  resetTime,
})(Stepcounter);
