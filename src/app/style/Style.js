import styled from 'styled-components';

const mainColor = 'rgb(0,121,121)';

export const Btnincrement = styled.div`
  width: 100px;
  height: 100px;
  background-color: green;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 22px;
  font-weight: bold;
  color: white;
  cursor: pointer;
  margin: 0 auto;
`;

export const Clicked = styled.div`
  font-size: 32px;
  font-weight: bold;
  width: 50px;
  height: 50px;
  display: flex;
  justify-content: center;
  margin: 0 auto;
`;

export const Btndecrement = styled.div`
    width: 50px;
    height: 50px;
    background-color: red;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 22px;
    font-weight: bold;
    color: white;
    cursor: pointer;
    margin: 100px auto 20px auto;
`;

export const Reset = styled.div`
    width: 50px;
    height: 50px;
    background-color: grey;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 14px;
    font-weight: bold;
    color: white;
    cursor: pointer;
    margin: 0 auto;
`;

export const CounterRoot = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-item: center;
`;

export const Navbar = styled.div`
    width: 100%;
    background-color: ${mainColor};
    ul{
      list-style-type: none;
      width: 100%;
      display: flex;
      justify-content: space-around;
      padding: 0;
        li{
          a{
            color: white;
          }
        }
      }
`;


export const StepStyled = styled.div`
      display: flex;
      flex-direction: column;
      align-items: center;
      form {
        display: flex;
        flex-direction: column;
        label {
          display: flex;
          flex-direction: column;
          align-items: center;
          input {
            margin: 10px 0;
            border: none;
            border: 1px solid gray;
            width: 70px;
            height: 40px;
            border-radius: 5px;
            text-align: center;
            font-size: 2em;
            font-weight: bold;
          }
          input[name='start']{
            width: 140px;
            font-size: 1.4em;
            background-color: ${mainColor};
            color: white;
            display: flex;
            justify-content: center;
            cursor: pointer;
          }
        }
      }
      .add-btn{
        display: flex;
        height: 60px;
        width: 300px;
        justify-content: space-around;
        span{
          margin: 10px 0;
          border: none;
          border: 1px solid gray;
          min-height: 30px;
          border-radius: 5px;
          text-align: center;
          display: flex;
          align-items: center;
          justify-content: center;
          font-size: 2em;
          font-weight: bold;
          width: 140px;
          font-size: 1.4em;
          background-color: ${mainColor};
          color: white;
          cursor: pointer;
        }
      } 
`;
