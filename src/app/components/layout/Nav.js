import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar } from '../../style/Style';

export const Nav = () => (
  <Navbar>
    <ul>
      <li>
        <Link to="/labcounter">Lab counter</Link>
      </li>
      <li>
        <Link to="/yogacounter">Yoga counter</Link>
      </li>
    </ul>
  </Navbar>
);
